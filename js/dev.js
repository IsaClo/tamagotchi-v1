  // progress bar  gestion du sommeil
let elementimg = document.getElementById("avatar");
let elementimgheat = document.getElementById("heat");
let elementimgvertigo = document.getElementById("vertigo");
    elementimgheat.hidden = true;
    elementimgvertigo.hidden = true;

let countersom = 60;
let intervalIdSom = null;

function bipsom() {
    let elem = document.getElementById("myBarsom");
    let width = 100;
    if(countersom === 0 || width === 0) {
        finish();
    } else {    
        width=countersom / 0.6;  
        document.getElementById("bipsom").innerText = countersom + " secondes restantes";
        elem.style.width = width + '%';
        countersom--;
          if (countersom<40 && countersom>=20) {
              toast2();
              elem.style.backgroundColor="orange";
              elementimg.hidden = true;
              elementimgvertigo.hidden = true;
              elementimgheat.hidden = false;
              elementimgheat.classList.add('avatarfaim');
          } else if (countersom<20) {
              elem.style.backgroundColor="red";
              toast3();
              elementimg.hidden = true;
              elementimgheat.hidden = true;
              elementimgheat.classList.remove('avatarfaim');
              elementimgvertigo.hidden = false;
              elementimgvertigo.classList.add('avatarsuper');
      } 
    }
}

function initialisesom() {
    countersom = 60;
    document.getElementById("myBarsom").style.backgroundColor = '#07F26C';
    elementimgvertigo.hidden = true;
    elementimgheat.hidden = true;
    elementimg.hidden = false;
} 

// progress bar  gestion de la faim
let counterfaim = 40;
let intervalIdFaim = null;

function bipfaim() {
    let elem = document.getElementById("myBarfaim");
    let width = 100;

    if(counterfaim === 0 || width === 0) {
      finish();
    } else {      
        width=counterfaim / 0.4;  
        document.getElementById("bipfaim").innerText = counterfaim + " secondes restantes";
        elem.style.width = width + '%';
        counterfaim--;
          if (counterfaim<27 && counterfaim>=14) {
            toast();
            elem.style.backgroundColor="orange";
            elementimg.hidden = true;
            elementimgvertigo.hidden = true;
            elementimgheat.hidden = false;
            elementimgheat.classList.add('avatarfaim');
          } else if (counterfaim<14) {
            elem.style.backgroundColor="red";
            toast1();
            elementimg.hidden = true;
            elementimgheat.hidden = true;
            elementimgheat.classList.remove('avatarfaim');
            elementimgvertigo.hidden = false;
            elementimgvertigo.classList.add('avatarsuper');
      }
    }
}

function initialisefaim(){
    counterfaim = 40;
    document.getElementById("myBarfaim").style.backgroundColor = '#07F26C';
    elementimgvertigo.hidden = true;
    elementimgheat.hidden = true;
    elementimg.hidden = false;
}       

// progress bar  gestion de l'hygiène
let counterlav = 30;
let intervalIdlav = null;

function biplav() {
    let elem = document.getElementById("myBarlav");
    let width = 100;

    if(counterlav === 0 || width === 0) {
        finish();
    } else {      
        width=counterlav / 0.3;  
        document.getElementById("biplav").innerText = counterlav + " secondes restantes";
        elem.style.width = width + '%';
        counterlav--;
          if (counterlav<20 && counterlav>=10) {
          elem.style.backgroundColor="orange";
          elementimg.hidden = true;
           elementimgvertigo.hidden = true;
          elementimgheat.hidden = false;
          elementimgheat.classList.add('avatarfaim');
          toast4();
        } else if (counterlav<10) {
          elem.style.backgroundColor="red";
          toast5();
          elementimg.hidden = true;
          elementimgheat.hidden = true;
              elementimgheat.classList.remove('avatarfaim');
          elementimgvertigo.hidden = false;
          elementimgvertigo.classList.add('avatarsuper');
        }
    }   
}
function initialiselav(){
    counterlav = 30;
    document.getElementById("myBarlav").style.backgroundColor = '#07F26C';
    elementimgvertigo.hidden = true;
    elementimgheat.hidden = true;
    elementimg.hidden = false;
}    

//lancement du jeu
function start(){
    initialisesom();
    initialiselav();
    initialisefaim();
    intervalIdSom = setInterval(bipsom, 1000);
    intervalIdFaim = setInterval(bipfaim, 1000);
    intervalIdlav = setInterval(biplav, 1000);
  } 

//fin du jeu
function finish() {
    clearInterval(intervalIdSom);
    document.getElementById("bipsom").innerText = "TERMINE!";   
    document.getElementById("myBarsom").style.width = 0;
    clearInterval(intervalIdFaim);
    document.getElementById("bipfaim").innerHTML = "TERMINE!";  
    document.getElementById("myBarfaim").style.width = 0; 
    clearInterval(intervalIdlav);
    document.getElementById("biplav").innerHTML = "TERMINE!";  
    document.getElementById("myBarlav").style.width = 0;      
  }


// les alertes
function toast() {
  let s1 = document.getElementById("snackbar");
  s1.className = "show";
  setTimeout(function(){ s1.className = s1.className.replace("show", ""); }, 3000);
 }

function toast1() {
    let s2 = document.getElementById("snackbar1");
    s2.className = "show";
    setTimeout(function(){ s2.className = s2.className.replace("show", ""); }, 3000);
}

function toast2() {
    let s3 = document.getElementById("snackbarsom");
    s3.className = "show";
    setTimeout(function(){ s3.className = s3.className.replace("show", ""); }, 3000);
}

function toast3() {
    let s4 = document.getElementById("snackbar1som");
    s4.className = "show";
    setTimeout(function(){ s4.className = s4.className.replace("show", ""); }, 3000);
}

function toast4() {
    let s5 = document.getElementById("snackbarlav");
    s5.className = "show";
    setTimeout(function(){ s5.className = s5.className.replace("show", ""); }, 3000);
}

function toast5() {
    let s6 = document.getElementById("snackbar1lav");
    s6.className = "show";
    setTimeout(function(){ s6.className = s6.className.replace("show", ""); }, 3000);
}


// les commandes pour 'grands écrans

let hu = document.getElementById("commandes");
    hu.focus();
    hu.addEventListener('keydown', function (e){
       switch(e.keyCode) {
          case  'S'.charCodeAt(0) :
             start();
             document.getElementById("commandes").value = '';
             hu.focus();
             break;

          case 'N'.charCodeAt(0) :
             initialisefaim();
             document.getElementById("commandes").value = '';
             hu.focus();
             break;

          case 'L'.charCodeAt(0):
             initialiselav();
             document.getElementById("commandes").value = '';
             hu.focus();
             break;

          case 'D'.charCodeAt(0):
              initialisesom();
              document.getElementById("commandes").value = '';
              hu.focus();
              break;

          default:
              alert ('la valeur n\'est pas bonne!');
              document.getElementById("commandes").value = '';
       }

    });
  // les commandes pour 'petits écrans

  let hupt = document.getElementById("commandespt");
  hupt.focus();
  hupt.addEventListener('keydown', function (e){
      switch(e.keyCode) {
          case  'S'.charCodeAt(0) :
              start();
              document.getElementById("commandespt").value = '';
              hupt.focus();
              break;

          case 'N'.charCodeAt(0) :
              initialisefaim();
              document.getElementById("commandespt").value = '';
              hupt.focus();
              break;

          case 'L'.charCodeAt(0):
              initialiselav();
              document.getElementById("commandespt").value = '';
              hupt.focus();
              break;

          case 'D'.charCodeAt(0):
              initialisesom();
              document.getElementById("commandespt").value = '';
              hupt.focus();
              break;

          default:
              alert ('la valeur n\'est pas bonne!');
              document.getElementById("commandespt").value = '';
      }

  });
/*
  function getHU()
  {
      var hu = document.getElementById("commandes").value;
      if (hu!='') {
        switch (hu) {
        case 's' :
          start();
          document.getElementById("commandes").value = '';
          break;
      
        case 'n' : 
          initialisefaim();
          document.getElementById("commandes").value = '';
          break;
  
        case 'l':
          initialiselav();
          document.getElementById("commandes").value = '';
          break;

        case 'd':
          initialisesom();
          document.getElementById("commandes").value = '';
          break;
    
        default:
        alert ('la valeur n\'est pas bonne!');
        document.getElementById("commandes").value = '';
     }
    }
  }*/

